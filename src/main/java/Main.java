import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;


/**
 * Created by joy on 2/22/17.
 */
public class Main {
    public static void main(String[] args) {
//Main Classs
//Using Docker Java API 
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost("tcp://192.168.100.22:2375")
                .build();

        DockerClient dockerClient = DockerClientBuilder.getInstance(config)
                .build();

        Info info = dockerClient.infoCmd().exec();

//storing docker version in string format
        String dockerVersion = info.getServerVersion();

//print version of docker you are using
        System.out.println(dockerVersion);


    }
}
